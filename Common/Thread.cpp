#include "Common.h"
#include "Thread.h"

unsigned int _stdcall Callback( void* inParame )
{
	( ( Thread* )inParame )->Run();
	return 0;
}

Thread::Thread()
	: mIsRun( false )
	, mHandle( INVALID_HANDLE_VALUE )
{
}

void Thread::Start()
{
	mIsRun = true;
	mHandle = ( HANDLE )_beginthreadex( nullptr, 0, &Callback, this, 0, nullptr );
}

void Thread::Stop()
{
	mIsRun = false;
}

void Thread::SetHandle( HANDLE inHandle )
{
	mHandle = inHandle;
}

DWORD Thread::GetID() const
{
	return GetThreadId( mHandle );
}

bool Thread::IsRun() const
{
	return mIsRun;
}
