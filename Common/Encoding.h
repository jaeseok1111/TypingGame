#pragma once
#include <locale>
#include <codecvt>

namespace Encoding {

inline std::string		UnicodeToMultibyte( const wchar_t* inString )
{
	int len = -1;
	int chars = WideCharToMultiByte( CP_ACP, 0, inString, len, nullptr, 0, nullptr, nullptr );

	std::string result;
	result.resize( chars );
	WideCharToMultiByte( CP_ACP, 0, inString, len, const_cast< char* >( result.c_str() ), chars, nullptr, nullptr );

	return result;
}

inline std::wstring		MultibyteToUnicode( const char* inString )
{
	int len = -1;
	int chars = MultiByteToWideChar( CP_ACP, 0, inString, len, nullptr, 0 );

	std::wstring result;
	result.resize( chars );
	MultiByteToWideChar( CP_ACP, 0, inString, len, const_cast< wchar_t* >( result.c_str() ), chars );

	return result;
}

}