#pragma once

class Thread
{
public:
	Thread();

	virtual void	Run() = 0;

	void			Start();
	void			Stop();

	void			SetHandle( HANDLE inHandle );
	DWORD			GetID() const;

	bool			IsRun() const;

protected:
	bool			mIsRun;
	HANDLE			mHandle;
};

using ThreadPtr = std::shared_ptr<Thread>;