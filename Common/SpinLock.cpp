#include "Common.h"
#include "SpinLock.h"

SpinLock::SpinLock()
	: mLockFlag( 0 )
{
}

void SpinLock::EnterLock()
{
	for ( int nloops = 0; ; nloops++ )
	{
		if ( InterlockedExchange( &mLockFlag, 1 ) == 0 )
			return;

		UINT uTimerRes = 1;
		timeBeginPeriod( uTimerRes );
		Sleep( ( DWORD )std::min( 10, nloops ) );
		timeEndPeriod( uTimerRes );
	}
}

void SpinLock::LeaveLock()
{
	InterlockedExchange( &mLockFlag, 0 );
}