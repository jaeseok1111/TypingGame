#pragma once
#include "SharedPacketID.h"

class IPacket
{
public:
	virtual void	Read( InputMemoryStream& inStream ) = 0;
	virtual void	Write( OutputMemoryStream& inStream ) = 0;
};

class LoginPacket : public IPacket
{
public:
	void			Read( InputMemoryStream& inStream ) override;
	void			Write( OutputMemoryStream& inStream ) override;

	std::string		Nickname;
};

class MessagePacket : public IPacket
{
public:
	void			Read( InputMemoryStream& inStream ) override;
	void			Write( OutputMemoryStream& inStream ) override;

	std::string		Message;
};

struct RankerPacket : public IPacket
{
public:
	void			Read( InputMemoryStream& inStream ) override;
	void			Write( OutputMemoryStream& inStream ) override;

	std::string		Nickname;
	
	float			Speed;
	float			Accuracy;
};

struct RankGroupPacket : public IPacket
{
public:
	void			Read( InputMemoryStream& inStream ) override;
	void			Write( OutputMemoryStream& inStream ) override;

	using RankerVector = std::vector<RankerPacket>;
	RankerVector	Rankings;
};