#pragma once
#include <mutex>

template <typename T>
class Singleton
{
public:

	static T* Get()
	{
		std::call_once(mFlag, []() {
			mInstance = std::make_shared<T>();
		});

		return mInstance.get();
	}

private:
	static std::once_flag		mFlag;
	static std::shared_ptr<T>	mInstance;
};


template <typename T>
std::once_flag Singleton<T>::mFlag;

template <typename T>
std::shared_ptr<T> Singleton<T>::mInstance = nullptr;