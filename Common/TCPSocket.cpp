#include "Common.h"
#include "TCPSocket.h"

TCPSocket::~TCPSocket()
{
	Close();
}

int TCPSocket::Connect( const SocketAddress & inAddress )
{
	int error = connect( mSocket, &inAddress.mSockAddr, static_cast< int >( inAddress.GetSize() ) );
	if ( error < 0 )
	{
		SocketUtil::ReportError( "TCPSocket::Connect" );
		return -SocketUtil::GetLastError();
	}

	return NO_ERROR;
}

int TCPSocket::Bind( const SocketAddress & inAddress )
{
	int error = bind( mSocket, &inAddress.mSockAddr, static_cast< int >( inAddress.GetSize() ) );
	if ( error != 0 )
	{
		SocketUtil::ReportError( "TCPSocket::Bind" );
		return -SocketUtil::GetLastError();
	}

	return NO_ERROR;
}

int TCPSocket::Listen( int inBackLog )
{
	int error = listen( mSocket, inBackLog );
	if ( error != 0 )
	{
		SocketUtil::ReportError( "TCPSocket::Listen" );
		return -SocketUtil::GetLastError();
	}

	return NO_ERROR;
}

void TCPSocket::Close()
{
	if ( mSocket == INVALID_SOCKET )
		return;

	closesocket( mSocket );
	mSocket = INVALID_SOCKET;
}

int32_t TCPSocket::Send( const void * inData, size_t inLength )
{
	int bytesSentCount = send( mSocket, static_cast< const char* >( inData ), static_cast< int >( inLength ), 0 );
	if ( bytesSentCount < 0 )
	{
		SocketUtil::ReportError( "TCPSocket::Send" );
		return -SocketUtil::GetLastError();
	}

	return bytesSentCount;
}

int32_t TCPSocket::Receive( void * inBuffer, size_t inLength )
{
	int bytesReceivedCount = recv( mSocket, static_cast< char* >( inBuffer ), static_cast< int >( inLength ), 0 );
	if ( bytesReceivedCount < 0 )
	{
		SocketUtil::ReportError( "TCPSocket::Receive" );
		return -SocketUtil::GetLastError();
	}

	return bytesReceivedCount;
}
