#pragma once
#include "SocketAddress.h"

class TCPSocket
{
public:
	~TCPSocket();

	int			Connect( const SocketAddress& inAddress );
	int			Bind( const SocketAddress& inAddress );
	int			Listen( int inBackLog );

	void		Close();
	
	int32_t		Send( const void* inData, size_t inLength );
	int32_t		Receive( void* inBuffer, size_t inLength );

	SOCKET		GetHandle() const { return mSocket; }

	bool		IsValid() const { return mSocket != INVALID_SOCKET; }

private:
	friend class SocketUtil;
	TCPSocket( SOCKET inSocket ) : mSocket( inSocket ) {}

	SOCKET		mSocket;
};

using TCPSocketPtr = std::shared_ptr<TCPSocket>;