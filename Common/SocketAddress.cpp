#include "Common.h"
#include "SocketAddress.h"

SocketAddress::SocketAddress()
{
	GetAsSockAddrIn()->sin_family = AF_INET;
	GetIP4Ref() = INADDR_ANY;
	GetAsSockAddrIn()->sin_port = 0;
}

SocketAddress::SocketAddress( const SOCKADDR_IN & addr )
{
	memcpy( &mSockAddr, &addr, sizeof( SOCKADDR ) );
}

SocketAddress::SocketAddress( const char* inAddress, uint16_t inPort )
{
	GetAsSockAddrIn()->sin_family = AF_INET;
	GetIP4Ref() = inet_addr( inAddress );
	GetAsSockAddrIn()->sin_port = htons( inPort );
}

string SocketAddress::ToString()
{
	auto addr = GetAsSockAddrIn();
	return StringUtil::Sprintf( "%s:%d\n", inet_ntoa( addr->sin_addr ), ntohs( addr->sin_port ) );
}
