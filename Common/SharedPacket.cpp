#include "Common.h"
#include "SharedPacket.h"

void LoginPacket::Read( InputMemoryStream & inStream )
{
	inStream.Read( Nickname );
}

void LoginPacket::Write( OutputMemoryStream & inStream )
{
	inStream.Write( Nickname );
}

void MessagePacket::Read( InputMemoryStream & inStream )
{
	inStream.Read( Message );
}

void MessagePacket::Write( OutputMemoryStream & inStream )
{
	inStream.Write( Message );
}

void RankerPacket::Read( InputMemoryStream & inStream )
{
	inStream.Read( Nickname );

	inStream.Read( Speed );
	inStream.Read( Accuracy );
}

void RankerPacket::Write( OutputMemoryStream & inStream )
{
	inStream.Write( Nickname );

	inStream.Write( Speed );
	inStream.Write( Accuracy );
}

void RankGroupPacket::Read( InputMemoryStream & inStream )
{
	int32_t elementCount;
	inStream.Read( elementCount );

	Rankings.resize( elementCount );

	for ( auto& ranker : Rankings )
		ranker.Read( inStream );
}

void RankGroupPacket::Write( OutputMemoryStream & inStream )
{
	inStream.Write( static_cast< int32_t >( Rankings.size() ) );

	for ( auto ranker : Rankings )
		ranker.Write( inStream );
}