#pragma once
#include "Common.h"
#include "MemoryStream.h"

template <typename T> 
inline void OutputMemoryStream::Write( T inData )
{
	static_assert( std::is_arithmetic<T>::value || std::is_enum<T>::value,
				   "이 함수는 기본 데이터 형식만을 지원합니다" );

	Write( &inData, sizeof( inData ) );
}

template<typename T>
inline void OutputMemoryStream::Write( const std::vector<T>& inVector )
{
	size_t elementCount = inVector.size();
	Write( elementCount );

	for ( const T& element : inVector )
		Write( element );
}

template<typename T>
inline void InputMemoryStream::Read( T& outData )
{
	static_assert( std::is_arithmetic<T>::value || std::is_enum<T>::value,
				   "이 함수는 기본 데이터 형식만을 지원합니다" );

	Read( &outData, sizeof( outData ) );
}

template<typename T>
inline void InputMemoryStream::Read( std::vector<T>& outVector )
{
	size_t elementCount;
	Read( elementCount );

	outVector.resize( elementCount );

	for ( T& element : outVector )
		Read( element );
}
