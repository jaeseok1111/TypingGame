#pragma once

class IPacket;

class OutputMemoryStream
{
public:
	OutputMemoryStream();
	~OutputMemoryStream();

	const char*					GetBufferPtr() const { return mBuffer; }

	uint32_t					GetLength() const { return mHead; }

	void						Write( const void* inData, size_t inByteCount );
	void						Write( const std::string& inString );

	template <typename T> void	Write( T inData );
	template <typename T> void	Write( const std::vector<T>& inVector );

private:
	void						ReallocBuffer( int newLength );

	char*						mBuffer;
	
	uint32_t					mHead;
	uint32_t					mCapacity;
};

class InputMemoryStream
{
public:
	InputMemoryStream();
	InputMemoryStream( char* inBuffer, int32_t inByteCount );
	~InputMemoryStream();

	uint32_t					GetRemainingDataSize() const { return mCapacity - mHead; }

	void						Read( void* outData, uint32_t inByteCount );
	void						Read( std::string& outString );

	template <typename T> void	Read( T& outData );
	template <typename T> void	Read( std::vector<T>& outVector );

private:
	char*						mBuffer;

	uint32_t					mHead;
	uint32_t					mCapacity;
};

using OutputMemoryStreamPtr = std::shared_ptr<OutputMemoryStream>;
using InputMemoryStreamPtr = std::shared_ptr<InputMemoryStream>;

#include "MemoryStream.inl"