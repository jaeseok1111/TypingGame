#pragma once
#include <string>

namespace StringUtil {

inline std::string Sprintf( const char* inFormat, ... )
{
	static char temp[4096];

	va_list args;
	va_start( args, inFormat );

	_vsnprintf_s( temp, 4096, 4096, inFormat, args );
	return std::string( temp );
}

inline void Log( const char* inFormat, ... )
{
	static char temp[4096];

	va_list args;
	va_start( args, inFormat );

	_vsnprintf_s( temp, 4096, 4096, inFormat, args );

	printf( "%s\n", temp );
	OutputDebugStringA( temp );
	OutputDebugStringA( "\n" );
}

}

#define LOG(...)	StringUtil::Log(__VA_ARGS__);