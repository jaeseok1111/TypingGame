#pragma once

class SocketAddress
{
public:
	SocketAddress();
	SocketAddress( const SOCKADDR_IN& addr );
	SocketAddress( const char* inAddress, uint16_t inPort );

	uint32_t			GetSize() const { return sizeof( SOCKADDR ); };
	
	std::string			ToString();

private:
	friend class TCPSocket;

	SOCKADDR mSockAddr;

	uint32_t&			GetIP4Ref() { return *reinterpret_cast< uint32_t* >( &GetAsSockAddrIn()->sin_addr.S_un.S_addr ); }
	const uint32_t&		GetIP4Ref() const { return *reinterpret_cast< const uint32_t* >( &GetAsSockAddrIn()->sin_addr.S_un.S_addr ); }

	SOCKADDR_IN*		GetAsSockAddrIn() { return reinterpret_cast< SOCKADDR_IN* >( &mSockAddr ); }
	const SOCKADDR_IN*	GetAsSockAddrIn() const { return reinterpret_cast< const SOCKADDR_IN* >( &mSockAddr ); }
};