#pragma once
#include <time.h>
#include <random>
#include <functional>

#include "Singleton.h"

class Random : public Singleton<Random>
{
public:
	template <typename T>
	static T					Range( T min, T max )
	{
		std::uniform_real_distribution<T> distribution( min, max );
		return distribution( Get()->mDevice );
	}

	template <>
	static int					Range<int>( int min, int max )
	{
		std::uniform_int_distribution<int> distribution( min, max );
		return distribution( Get()->mDevice );
	}
	
private:
	std::random_device			mDevice;
};