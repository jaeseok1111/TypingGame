#pragma once
#include "Singleton.h"

class FileSystem
{
public:
	static std::string LoadFile( const std::string& inPath )
	{
		std::ifstream stream( inPath );
		std::stringstream sstream;

		sstream << stream.rdbuf();
		return sstream.str();
	}

	static std::wstring LoadFile( const std::wstring& inPath )
	{
		std::wifstream stream( inPath );
		stream.imbue( std::locale( std::locale::empty(), new std::codecvt_utf8<wchar_t, 0x10ffff, std::consume_header> ) );

		std::wstringstream sstream;
		sstream << stream.rdbuf();

		return sstream.str();
	}
};

