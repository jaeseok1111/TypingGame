#include "Common.h"
#include "MemoryStream.h"

OutputMemoryStream::OutputMemoryStream()
	: mHead( 0 )
{
	ReallocBuffer( 32 );
}

OutputMemoryStream::~OutputMemoryStream()
{
	std::free( mBuffer );
}

void OutputMemoryStream::ReallocBuffer( int newLength )
{
	mBuffer = static_cast< char* >( std::realloc( mBuffer, newLength ) );

	mCapacity = newLength;
}

void OutputMemoryStream::Write( const void* inData, size_t inByteCount )
{
	uint32_t newHead = mHead + static_cast< uint32_t >( inByteCount );
	if ( newHead < mCapacity )
	{
		ReallocBuffer( std::max( mCapacity * 2, newHead ) );
	}

	std::memcpy( mBuffer + mHead, inData, inByteCount );
	
	mHead = newHead;
}

void OutputMemoryStream::Write( const std::string & inString )
{
	size_t elementCount = inString.size();
	Write( elementCount );
	Write( inString.data(), elementCount * sizeof( char ) );
}

InputMemoryStream::InputMemoryStream()
	: mBuffer( nullptr )
	, mCapacity( 0 )
	, mHead( 0 )
{
}

InputMemoryStream::InputMemoryStream( char* inBuffer, int32_t inByteCount )
	: mBuffer( new char[inByteCount] )
	, mCapacity( inByteCount )
	, mHead( 0 )
{
	std::memcpy( mBuffer, inBuffer, inByteCount );
}

InputMemoryStream::~InputMemoryStream()
{
	std::free( mBuffer );
}

void InputMemoryStream::Read( void* outData, uint32_t inByteCount )
{
	uint32_t newHead = mHead + inByteCount;
	assert( newHead <= mCapacity );

	std::memcpy( outData, mBuffer + mHead, inByteCount );

	mHead = newHead;
}

void InputMemoryStream::Read( std::string& outString )
{
	std::vector<char> str;
	Read( str );

	for ( auto ch : str )
		outString.push_back( ch );
}
