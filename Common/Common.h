#pragma once

#include <SDKDDKVer.h>

#define _WINSOCK_DEPRECATED_NO_WARNINGS
#define _SILENCE_CXX17_CODECVT_HEADER_DEPRECATION_WARNING

// Use the C++ standard templated min/max
#define NOMINMAX

// Include <mcx.h> if you need this
#define NOMCX

// Include <winsvc.h> if you need this
#define NOSERVICE

// WinHelp is deprecated
#define NOHELP

#include <WinSock2.h>
#include <WS2tcpip.h>
#include <MSWSock.h>
#include <Windows.h>

#include <wrl/client.h>

#include <stdarg.h>

#include <iomanip>
#include <iostream>
#include <assert.h>
#include <process.h>
#include <conio.h>
#include <stdlib.h>
#include <thread>
#include <map>
#include <queue>
#include <vector>
#include <functional>
#include <unordered_map>
#include <algorithm>
#include <exception>
#include <memory>
#include <stdexcept>
#include <sstream>
#include <fstream>
#include <cwctype>
#include <clocale>
#include <chrono>

#include "Random.h"
#include "Encoding.h"
#include "FileSystem.h"

#include "StringUtil.h"

#include "SocketUtil.h"
#include "SocketAddress.h"
#include "TCPSocket.h"

#include "MemoryStream.h"

#include "ILock.h"
#include "SpinLock.h"

#include "Thread.h"
#include "Singleton.h"

#include "SharedPacketID.h"
#include "SharedPacket.h"

using namespace Microsoft;
using namespace Microsoft::WRL;

using namespace std;