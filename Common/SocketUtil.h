#pragma once
#include "TCPSocket.h"

enum SocketAddressFamily
{
	Inet = AF_INET,
	Inet6 = AF_INET6
};

class SocketUtil
{
public:
	static TCPSocketPtr CreateTCPSocket( SocketAddressFamily inFamily )
	{
		SOCKET sock = socket( inFamily, SOCK_STREAM, IPPROTO_TCP );

		if ( sock == INVALID_SOCKET )
		{
			ReportError( "SocketUtils::CreateTCPSocket" );
			return nullptr;
		}

		return TCPSocketPtr( new TCPSocket( sock ) );
	}

	static void ReportError( const char* inDesc )
	{
		TCHAR* message = nullptr;
		DWORD error = GetLastError();

		FormatMessage( FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_ALLOCATE_BUFFER,
					   nullptr,
					   error,
					   MAKELANGID( LANG_NEUTRAL, SUBLANG_DEFAULT ),
					   ( TCHAR * )&message,
					   0,
					   nullptr );

		LOG( "Error: %s: %d- %s\n", inDesc, error, Encoding::UnicodeToMultibyte( message ).c_str() );
		LocalFree( message );
	}

	static int GetLastError()
	{
		return WSAGetLastError();
	}
};