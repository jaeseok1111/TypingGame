#pragma once
#include "ILock.h"

class SpinLock : public ILock
{
public:
	SpinLock();

	void			EnterLock() override;
	void			LeaveLock() override;

private:
	volatile long	mLockFlag;
};

