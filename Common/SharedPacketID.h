#pragma once

enum class PacketID
{
	Invalid = -1,
	Login,
	Message,
	SystemMessage,

	ShowRanking,
};