#pragma once

class ILock
{
protected:
	ILock() = default;

public:
	virtual void EnterLock() = 0;
	virtual void LeaveLock() = 0;

private:
	ILock( const ILock& ) = default;
	ILock& operator =( const ILock& ) = default;
};

class LockGuard
{
public:
	LockGuard( ILock* inLock ) : mLock( inLock )
	{
		mLock->EnterLock();
	}

	~LockGuard()
	{
		mLock->LeaveLock();
	}

private:
	ILock* mLock;
};