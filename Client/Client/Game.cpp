#include "stdafx.h"
#include "Networking.h"
#include "Game.h"


Game::Game( const std::string& title, int width, int height )
	: mIsRun( false )
	, mSwapChain( std::make_shared<SwapChain>( width, height ) )
	, mNetworkReceiver( nullptr )
{
	SetConsoleTitleA( title.c_str() );

	mSwapChain->ShowCursor( false );
}

void Game::Run( ViewPtr viewer )
{
	mIsRun = true;

	while ( mIsRun )
	{
		mSwapChain->Clear();

		Update( viewer );

		mSwapChain->Flipping();
	}
}

void Game::Stop()
{
	mIsRun = false;
	mNetworkReceiver = nullptr;
}

void Game::Update( ViewPtr viewer )
{
	NetworkMessageProcess( viewer );

	if ( _kbhit() )
	{
		wchar_t ch = _getwch();

		if ( ch == 27 ) // esc
		{
			Stop();
			return;
		}

		viewer->InputUpdate( ch );
	}

	viewer->Update();
	viewer->Render( mSwapChain );
}

void Game::NetworkMessageProcess( ViewPtr viewer )
{
	InputMemoryStreamPtr stream;

	while ( Networking::Get()->Pop( stream ) )
	{
		if ( mNetworkReceiver == nullptr )
			continue;

		PacketID packetID;
		stream->Read( packetID );

		mNetworkReceiver->OnMessage( packetID, *stream );
	}
}