#include "stdafx.h"
#include "Networking.h"

class ReadMessageThread : public Thread
{
public:
	ReadMessageThread( Networking* inNetwork ) : mNetwork( inNetwork ) {}

	void Run() override
	{
		char recvBuffer[1024];
		int length;

		TCPSocket socket = mNetwork->GetSocket();

		while ( IsRun() )
		{
			length = socket.Receive( recvBuffer, 1024 );

			if ( length <= 0 )
				return;

			mNetwork->Push( make_shared<InputMemoryStream>( recvBuffer, length ) );
		}
	}

private:
	Networking * mNetwork;
};

Networking::Networking()
	: mSocket( nullptr )
	, mReadMessageThread( nullptr )
{
	WSADATA data;
	WSAStartup( MAKEWORD( 2, 2 ), &data );
}

Networking::~Networking()
{
	if ( mReadMessageThread == nullptr )
		return;

	mReadMessageThread->Stop();
}

bool Networking::Connect( const char* ip, uint16_t port )
{
	mSocket = SocketUtil::CreateTCPSocket( SocketAddressFamily::Inet );
	if ( mSocket == nullptr )
		return false;

	if ( NO_ERROR != mSocket->Connect( SocketAddress( ip, port ) ) )
		return false;

	mReadMessageThread = std::make_shared<ReadMessageThread>( this );
	mReadMessageThread->Start();
	
	return true;
}

void Networking::Send( OutputMemoryStream & inStream )
{
	mSocket->Send( inStream.GetBufferPtr(), inStream.GetLength() );
}

void Networking::Send( PacketID inPacketID, IPacket* inPacket )
{
	OutputMemoryStream stream;
	stream.Write( inPacketID );
	
	inPacket->Write( stream );

	Send( stream );
}

void Networking::Push( InputMemoryStreamPtr inStream )
{
	LockGuard guard( &mLock );

	if ( inStream->GetRemainingDataSize() == 0 )
		return;

	mMessages.push( inStream );
}

bool Networking::Pop( InputMemoryStreamPtr& inStream )
{
	LockGuard guard( &mLock );

	if ( mMessages.empty() )
		return false;

	inStream = mMessages.front();
	mMessages.pop();
	return true;
}

bool Networking::Empty()
{
	LockGuard guard( &mLock );

	return mMessages.empty();
}