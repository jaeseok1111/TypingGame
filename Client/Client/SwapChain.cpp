#include "stdafx.h"
#include "SwapChain.h"

SwapChain::SwapChain( int width, int height, int bufferCount )
	: mBufferIndex( 0 )
{
	for ( int i = 0; i < bufferCount; ++i )
		mBuffers.push_back( CreateScreenBuffer() );
}

void SwapChain::Draw( const std::string& text, int x, int y )
{
	SetPosition( x, y );
	WriteFile( mBuffers[mBufferIndex], text.c_str(), static_cast< DWORD >( text.length() ), 0, nullptr );
}

void SwapChain::Draw( const std::wstring& text, int x, int y )
{
	Draw( Encoding::UnicodeToMultibyte( text.c_str() ), x, y );
}

void SwapChain::Clear()
{
	DWORD dw;
	COORD zero = { 0, 0 };
	HANDLE buffer = mBuffers[mBufferIndex];

	FillConsoleOutputCharacter( mBuffers[mBufferIndex], ' ', mOutputWidth* mOutputHeight, zero, &dw );
}

void SwapChain::Flipping()
{
	Sleep( 33 );
	SetConsoleActiveScreenBuffer( mBuffers[mBufferIndex] );

	// mBufferIndex = 0 ~ 최대 버퍼 수 - 1
	mBufferIndex = std::clamp( mBufferIndex + 1, 0, static_cast< int >( mBuffers.size() - 1 ) );
}

void SwapChain::ShowCursor( bool isVisible )
{
	for ( auto buffer : mBuffers )
	{
		CONSOLE_CURSOR_INFO cursorInfo;
		GetConsoleCursorInfo( buffer, &cursorInfo );

		cursorInfo.bVisible = isVisible;
		SetConsoleCursorInfo( buffer, &cursorInfo );
	}
}

void SwapChain::SetPosition( int x, int y )
{
	COORD cursorPosition = { static_cast< short >( x ), static_cast< short >( y ) };
	SetConsoleCursorPosition( mBuffers[mBufferIndex], cursorPosition );
}

int SwapChain::GetWidth() const
{
	return mOutputWidth;
}

int SwapChain::GetHeight() const
{
	return mOutputHeight;
}

HANDLE SwapChain::CreateScreenBuffer()
{
	HANDLE result = CreateConsoleScreenBuffer( GENERIC_READ | GENERIC_WRITE, 0, nullptr, CONSOLE_TEXTMODE_BUFFER, nullptr );
	assert( result != INVALID_HANDLE_VALUE );
	return result;
}
