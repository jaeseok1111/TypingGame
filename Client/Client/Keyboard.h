#pragma once

class SwapChain;

class Keyboard
{
public:
	using Event = std::function<void( std::wstring& )>;

	Keyboard();

	void				AddEvent( wchar_t key, Event callback );

	void				ClearData();

	void				Update( wchar_t ch );

	void				Draw( std::shared_ptr<SwapChain> swapChain, int x, int y );

	const std::wstring& GetData() const { return mInputData; }

	bool				IsEmpty() const { return mInputData.empty(); }

private:
	bool				OnEvent( wchar_t ch );

	bool				IsASCII( wchar_t ch );

	int					mCursorPositoin;
	std::wstring		mInputData;

	using EventMap = std::unordered_map<wchar_t, Event>;
	EventMap			mEvents;
};

using KeyboardPtr = std::shared_ptr<Keyboard>;