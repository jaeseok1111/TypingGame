#pragma once
#include "IView.h"
#include "Keyboard.h"

class Game;

class InputNicknameView : public IView
{
public:
	InputNicknameView( Game* game );

	void			Render( std::shared_ptr<SwapChain> swapChain ) override;
	void			InputUpdate( wchar_t ch ) override;

private:
	void			OnInputEnterKey( std::wstring& inputData );
	void			OnInputEraseKey( std::wstring& inputData );

	Game*			mGame;
	
	KeyboardPtr		mKeyboard;
};
