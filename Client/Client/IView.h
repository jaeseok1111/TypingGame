#pragma once
#include "SwapChain.h"

class IView
{
public:
	virtual void Render( std::shared_ptr<SwapChain> swapChain ) {}

	virtual void Update() {}
	virtual void InputUpdate( const wchar_t ch ) {}
};

using ViewPtr = std::shared_ptr<IView>;