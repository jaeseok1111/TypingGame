#include "stdafx.h"
#include "Game.h"
#include "Networking.h"
#include "InputNicknameView.h"

InputNicknameView::InputNicknameView( Game* game )
	: mGame( game )
	, mKeyboard(make_shared<Keyboard>())
{
	game->SetNetworkReceiver( nullptr );

	mKeyboard->AddEvent( L'\r', [ this ] ( std::wstring& inString ) { OnInputEnterKey( inString ); } );
	mKeyboard->AddEvent( L'\b', [ this ] ( std::wstring& inString ) { OnInputEraseKey( inString ); } );
}

void InputNicknameView::Render( std::shared_ptr<SwapChain> swapChain )
{
	swapChain->Draw( L"닉네임을 입력해주세요", 0, 0 );
	
	mKeyboard->Draw( swapChain, 0, 1 );
}

void InputNicknameView::InputUpdate( wchar_t ch )
{
	mKeyboard->Update( ch );
}

void InputNicknameView::OnInputEnterKey( std::wstring& inputData )
{
	LoginPacket packet;
	packet.Nickname = Encoding::UnicodeToMultibyte( mKeyboard->GetData().c_str() );

	Networking::Get()->Send( PacketID::Login, &packet );

	mGame->Stop();
}

void InputNicknameView::OnInputEraseKey( std::wstring & inputData )
{
	if ( inputData.empty() )
		return;

	inputData.pop_back();
}
