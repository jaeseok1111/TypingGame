#pragma once

class NetworkReceiver
{
	using OnNetworkMessage = std::function<void( InputMemoryStream& )>;
	using OnNetworkMessageEvent = std::unordered_map<PacketID, OnNetworkMessage>;

public:
	NetworkReceiver() = default;

	void					AddEvent( PacketID inPacketID, OnNetworkMessage inEvent );

	void					OnMessage( PacketID inPacketID, InputMemoryStream& inStream );

private:
	OnNetworkMessageEvent	mEvents;
};

using NetworkReceiverPtr = std::shared_ptr<NetworkReceiver>;