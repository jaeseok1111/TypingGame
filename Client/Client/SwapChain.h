#pragma once

class SwapChain
{
public:
	SwapChain( int width, int height, int bufferCount = 2 );

	void				Draw( const std::string& text, int x, int y );
	void				Draw( const std::wstring& text, int x, int y );

	void				Clear();
	void				Flipping();

	void				ShowCursor( bool isVisible );

	void				SetPosition( int x, int y );

	int					GetWidth() const;
	int					GetHeight() const;

private:
	HANDLE				CreateScreenBuffer();
	
	int					mOutputWidth;
	int					mOutputHeight;

	int					mBufferIndex;
	std::vector<HANDLE> mBuffers;
};

using SwapChainPtr = std::shared_ptr<SwapChain>;