#pragma once
#include "IView.h"
#include "NetworkReceiver.h"

#include "Keyboard.h"

class Game;

class MainView : public IView, public NetworkReceiver
{
public:
	MainView( Game* game );

	void						Render( std::shared_ptr<SwapChain> swapChain ) override;

	void						InputUpdate( wchar_t ch ) override;

private:
	// 키 입력 관련 이벤트
	void						OnInputEnterKey( std::wstring& inputData );
	void						OnInputEraseKey( std::wstring& inputData );

	// 네크워크 관련 이벤트
	void						OnMessage( InputMemoryStream& inStream );
	void						OnSystemMessage( InputMemoryStream& inStream );
	void						OnShowRanking( InputMemoryStream& inStream );

	KeyboardPtr					mKeyboard;

	std::vector<std::wstring>	mMessages;
};