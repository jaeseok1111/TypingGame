#include "stdafx.h"
#include "NetworkReceiver.h"

void NetworkReceiver::AddEvent( PacketID inPacketID, OnNetworkMessage inEvent )
{
	if ( mEvents.find( inPacketID ) != mEvents.end() )
		return;

	mEvents[inPacketID] = inEvent;
}

void NetworkReceiver::OnMessage( PacketID inPacketID, InputMemoryStream& inStream )
{
	if ( mEvents.find( inPacketID ) == mEvents.end() )
		return;

	mEvents[inPacketID]( inStream );
}
