#pragma once
class Networking : public Singleton<Networking>
{
public:
	Networking();
	~Networking();

	bool								Connect( const char* ip, uint16_t port );
	
	void								Send( OutputMemoryStream& inStream );
	void								Send( PacketID inPacketID, IPacket* inPacket );

	void								Push( InputMemoryStreamPtr inStream );
	bool								Pop( InputMemoryStreamPtr& inStream );

	bool								Empty();

	TCPSocket							GetSocket() { return *mSocket; }

private:
	TCPSocketPtr						mSocket;

	SpinLock							mLock;
	std::queue<InputMemoryStreamPtr>	mMessages;

	ThreadPtr							mReadMessageThread;
};

