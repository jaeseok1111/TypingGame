#include "stdafx.h"
#include "SwapChain.h"
#include "Keyboard.h"


Keyboard::Keyboard()
	: mCursorPositoin( 0 )
{
}

void Keyboard::AddEvent( wchar_t key, Event callback )
{
	if ( mEvents.find( key ) != mEvents.end() )
		return;

	mEvents[key] = callback;
}

void Keyboard::ClearData()
{
	mCursorPositoin = 0;
	mInputData.clear();
}

void Keyboard::Update( wchar_t ch )
{
	if ( OnEvent( ch ) )
		return;

	mCursorPositoin += IsASCII( ch ) ? 1 : 2;
	mInputData.push_back( ch );
}

void Keyboard::Draw( std::shared_ptr<SwapChain> swapChain, int x, int y )
{
	swapChain->Draw( mInputData, x, y );
	swapChain->SetPosition( x + mCursorPositoin, y );
}

bool Keyboard::OnEvent( wchar_t ch )
{
	if ( mEvents.find( ch ) == mEvents.end() )
		return false;

	mEvents[ch]( mInputData );
	return true;
}

bool Keyboard::IsASCII( wchar_t ch )
{
	return 0 >= ch && ch <= 255;
}
