#include "stdafx.h"
#include "Game.h"
#include "Networking.h"
#include "MainView.h"

MainView::MainView( Game* game )
	: mKeyboard( make_shared<Keyboard>() )
{
	game->SetNetworkReceiver( this );

	AddEvent( PacketID::Message, [ this ] ( InputMemoryStream& inStream ) { OnMessage( inStream ); } );
	AddEvent( PacketID::SystemMessage, [ this ] ( InputMemoryStream& inStream ) { OnSystemMessage( inStream ); } );
	AddEvent( PacketID::ShowRanking, [ this ] ( InputMemoryStream& inStream ) { OnShowRanking( inStream ); } );

	mKeyboard->AddEvent( L'\r', [ this ] ( std::wstring& inString ) { OnInputEnterKey( inString ); } );
	mKeyboard->AddEvent( L'\b', [ this ] ( std::wstring& inString ) { OnInputEraseKey( inString ); } );
}

void MainView::Render( std::shared_ptr<SwapChain> swapChain )
{
	int messagesIndex = 0;

	for ( auto message : mMessages )
		swapChain->Draw( message, 0, messagesIndex++ );

	mKeyboard->Draw( swapChain, 0, messagesIndex );
}

void MainView::InputUpdate( wchar_t ch )
{
	mKeyboard->Update( ch );
}

void MainView::OnInputEnterKey( std::wstring& inputData )
{
	MessagePacket packet;
	packet.Message = Encoding::UnicodeToMultibyte( mKeyboard->GetData().c_str() );

	Networking::Get()->Send( PacketID::Message, &packet );

	mKeyboard->ClearData();
}

void MainView::OnInputEraseKey( std::wstring & inputData )
{
	if ( inputData.empty() )
		return;

	inputData.pop_back();
}

void MainView::OnMessage( InputMemoryStream& inStream )
{
	MessagePacket packet;
	packet.Read( inStream );

	mMessages.push_back( Encoding::MultibyteToUnicode( packet.Message.c_str() ) );
}

void MainView::OnSystemMessage( InputMemoryStream& inStream )
{
	MessagePacket packet;
	packet.Read( inStream );

	mMessages.push_back( L"[System] " + Encoding::MultibyteToUnicode( packet.Message.c_str() ) );
}

void MainView::OnShowRanking( InputMemoryStream& inStream )
{
	RankGroupPacket packet;
	packet.Read( inStream );

	mMessages.push_back( L"" ); // empty line
	mMessages.push_back( L"[System] ��ŷ ���" );

	for ( size_t i = 0; i < packet.Rankings.size(); ++i )
	{
		auto ranker = packet.Rankings[i];

		auto message = std::to_wstring( i + 1 ) + L"��: " + Encoding::MultibyteToUnicode( ranker.Nickname.c_str() );
		auto accuracy = L" / " + std::to_wstring( ranker.Accuracy ) + L"%";
		auto speed = L" / " + std::to_wstring( ranker.Speed );

		mMessages.push_back( message.c_str() + accuracy + speed.c_str() );
	}

	mMessages.push_back( L"" ); // empty line
}