// Client.cpp: 콘솔 응용 프로그램의 진입점을 정의합니다.
//

#include "stdafx.h"
#include "Networking.h"
#include "Game.h"

#include "InputNicknameView.h"
#include "MainView.h"

#include <crtdbg.h>

std::unique_ptr<Game> game = nullptr;

BOOL WINAPI ConsoleShutdownHandler( DWORD dwCtrlEvent )
{
	switch ( dwCtrlEvent )
	{
		case CTRL_C_EVENT:
		case CTRL_BREAK_EVENT:
		case CTRL_CLOSE_EVENT:
		case CTRL_LOGOFF_EVENT:
		case CTRL_SHUTDOWN_EVENT:
		{
			if ( game != nullptr )
				game.reset();

			return TRUE;
		}
		default:
			return FALSE;
	}
}

int main()
{
	_CrtDumpMemoryLeaks();
	_CrtSetDbgFlag( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );
	_CrtSetReportMode( _CRT_ERROR, _CRTDBG_MODE_DEBUG );

	if ( SetConsoleCtrlHandler( ( PHANDLER_ROUTINE )ConsoleShutdownHandler, TRUE ) == FALSE )
	{
		LOG( "main::SetConsoleCtrlHandler Error!!" )
		return -1;
	}

	if ( false == Networking::Get()->Connect( "127.0.0.1", 20000 ) )
		return -1;

	game = make_unique<Game>( "TajaChat", 40, 80 );

	game->Run( std::make_shared<InputNicknameView>( game.get() ) );
	game->Run( std::make_shared<MainView>( game.get() ) );

	return 0;
}