#pragma once
#include "IView.h"
#include "SwapChain.h"

#include "NetworkReceiver.h"

class Game
{
public:
	Game( const std::string& title, int width, int height );

	void						Run( ViewPtr viewer );

	void						Stop();

	void						SetNetworkReceiver( NetworkReceiver* receiver ) { mNetworkReceiver = receiver; }

private:
	void						Update( ViewPtr viewer );

	void						NetworkMessageProcess( ViewPtr viewer );

	bool						mIsRun;
	SwapChainPtr				mSwapChain;

	NetworkReceiver*			mNetworkReceiver;
};

