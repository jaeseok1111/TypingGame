#pragma once

class Ranker
{
public:
	Ranker( const std::string& inNickname, const std::string& inMessage );

private:
	friend class RankingRecoder;

	void			GetAccuracy( const std::string& inAnswer );

	using TimePoint = std::chrono::high_resolution_clock::time_point;
	void			GetTypingPerMinute( TimePoint startTime );

	bool			IsHigherPriority( Ranker* ranker );

	RankerPacket	ToPacket();

	std::string		mNickname;
	std::string		mMessage;

	float			mSpeed;
	float			mAccuracy;

	TimePoint		mTypingEndTime;
};

using RankerPtr = std::shared_ptr<Ranker>;