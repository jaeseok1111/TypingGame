#include "stdafx.h"
#include "User.h"
#include "UserManager.h"
#include "LoginJob.h"

Job* LoginJob::Clone( Session * inSession )
{
	return new LoginJob( inSession );
}

bool LoginJob::OnRead( InputMemoryStream& stream )
{
	stream.Read( mNickname );

	return false == mNickname.empty();
}

void LoginJob::OnExecute()
{
	UserManager::Get()->AddNicknameOverlayCount( mNickname );

	auto globalNickname = UserManager::Get()->ConvertGlobalNickname( mNickname );
	( ( User* )mSession )->SetNickname( globalNickname );

	MessagePacket packet;
	packet.Message = globalNickname.c_str() + string( " ���� �����ϼ̽��ϴ�." );

	UserManager::Get()->Broadcast( PacketID::SystemMessage, &packet );
}
