#include "stdafx.h"
#include "Ranking.h"

void RankingRecoder::Record( const std::string& nickname, const std::string& message )
{
	LockGuard guard( &mLock );

	if ( mIsRecord == false )
		return;

	auto ranker = std::make_shared<Ranker>( nickname, message );
	ranker->GetAccuracy( mCurrentAnswer );
	ranker->GetTypingPerMinute( mStartTime );

	mRanking[mCurrentAnswer].push_back( ranker );

	std::sort( mRanking[mCurrentAnswer].begin(), mRanking[mCurrentAnswer].end(), [ this ] ( auto rank1, auto rank2 )
	{
		return rank1->IsHigherPriority( rank2.get() );
	} );
}

void RankingRecoder::Start( const std::string& inAnswer )
{
	LockGuard guard( &mLock );

	mIsRecord = true;
	mCurrentAnswer = inAnswer;

	mStartTime = chrono::high_resolution_clock::now();
}

void RankingRecoder::End()
{
	LockGuard guard( &mLock );

	mIsRecord = false;
}

std::vector<RankerPacket> RankingRecoder::GetRankingToAnswer()
{
	std::vector<RankerPacket> rankers;

	for ( auto ranker : mRanking[mCurrentAnswer] )
		rankers.push_back( ranker->ToPacket() );

	return rankers;
}

bool RankingRecoder::IsRecoding() const
{
	return mIsRecord;
}
