#pragma once
#include "Ranker.h"

class RankingRecoder : public Singleton<RankingRecoder>
{
public:
	void						Start( const std::string& inAnswer );
	void						End();

	void						Record( const std::string& nickname, const std::string& message );

	std::vector<RankerPacket>	GetRankingToAnswer();

	bool						IsRecoding() const;

private:
	bool						mIsRecord;

	SpinLock					mLock;

	std::string					mCurrentAnswer;

	using Ranking = std::vector<RankerPtr>;
	using RankingMap = std::map<std::string, Ranking>;
	RankingMap					mRanking;

	using TimePoint = std::chrono::high_resolution_clock::time_point;
	TimePoint					mStartTime;
};

