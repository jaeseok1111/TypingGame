// typing.cpp: 콘솔 응용 프로그램의 진입점을 정의합니다.
//

#include "stdafx.h"
#include "User.h"

#include "LoginJob.h"
#include "MessageJob.h"
#include "QuestionJob.h"

#include "QuestionManager.h"

#include <crtdbg.h>

class TypingServer : public Server
{
public:
	void Initialize( Session* inPrototype, uint16_t inPort ) override
	{
		Server::Initialize( inPrototype, inPort );

		QuestionManager::Get()->Load( L"Resources/Questions.txt" );

		OnCreateJob();
	}

private:
	void OnCreateJob()
	{
		auto jobManager = JobManager::Get();

		jobManager->AddJob( PacketID::Login, new LoginJob( nullptr ) );
		jobManager->AddJob( PacketID::Message, new ChatMessage( nullptr ) );

		jobManager->AddTimeJob( new QuestionWaitJob( nullptr, 10000 ) ); // 60초
	}
};

int main()
{
	_CrtDumpMemoryLeaks();
	_CrtSetDbgFlag( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );
	_CrtSetReportMode( _CRT_ERROR, _CRTDBG_MODE_DEBUG );

	if ( FAILED( CoInitializeEx( nullptr, COINITBASE_MULTITHREADED ) ) )
		return 1;

	TypingServer server;

	server.Initialize( new User(), 20000 );
	server.Run();
	
	CoUninitialize();

    return 0;
}

