#include "stdafx.h"
#include "Ranker.h"

Ranker::Ranker( const std::string & inNickname, const std::string & inMessage )
	: mNickname( inNickname )
	, mMessage( inMessage )
	, mSpeed( 0.0f )
	, mAccuracy( 0.0f )
{
	mTypingEndTime = chrono::high_resolution_clock::now();
}

RankerPacket Ranker::ToPacket()
{
	RankerPacket packet;
	packet.Nickname = mNickname;
	packet.Accuracy = mAccuracy;
	packet.Speed = mSpeed;

	return packet;
}

void Ranker::GetAccuracy( const std::string& inAnswer )
{
	auto answerLength = inAnswer.length();
	auto messageLength = mMessage.length();
	auto currectCount = answerLength;

	for ( auto i = 0; i < answerLength; ++i )
	{
		if ( i >= messageLength )
		{
			--currectCount;
			continue;
		}

		if ( inAnswer[i] == mMessage[i] )
			continue;

		--currectCount;
	}

	mAccuracy = static_cast<float>( currectCount ) / answerLength * 100.f;
}

void Ranker::GetTypingPerMinute( TimePoint startTime )
{
	auto miliseconds = chrono::duration_cast< chrono::milliseconds >( mTypingEndTime - startTime ).count();
	auto time = std::max( 1, static_cast< int >( miliseconds ) );

	mSpeed = mMessage.length() * 60000.0f / time;
}

bool Ranker::IsHigherPriority( Ranker* ranker )
{
	if ( mAccuracy > ranker->mAccuracy )
		return true;
	else
		return mSpeed > ranker->mSpeed;

	return false;
}
