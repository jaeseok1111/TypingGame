#include "stdafx.h"
#include <iterator>
#include "QuestionManager.h"

bool QuestionManager::Load( const std::wstring & path )
{
	auto text = Encoding::UnicodeToMultibyte( FileSystem::LoadFile( path ).c_str());
	
	std::string question;
	std::istringstream tokenStream( text );

	while ( std::getline( tokenStream, question, '\n') )
		mQuestions.push_back( question );

	return false == mQuestions.empty();
}

bool QuestionManager::CreateRandomAnswer()
{
	if ( mQuestions.empty() )
		return false;

	int index = Random::Range( 0, static_cast< int >( mQuestions.size() - 1 ) );

	mAnswer = mQuestions[index];
	return true;
}

const std::string& QuestionManager::NowAnswer()
{
	return mAnswer;
}