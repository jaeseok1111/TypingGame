#include "stdafx.h"
#include "User.h"
#include "UserManager.h"

void UserManager::AddUser( User * inUser )
{
	LockGuard guard( &mLock );

	if ( inUser == nullptr )
		return;

	mUsers.push_back( inUser );
}

void UserManager::RemoveUser( User * inUser )
{
	LockGuard guard( &mLock );

	int machineID = inUser->GetMachineID();

	mUsers.erase( std::remove_if( mUsers.begin(), mUsers.end(), [ machineID ] ( auto user )
	{
		return user->GetMachineID() == machineID;

	} ), mUsers.end() );
}

void UserManager::Broadcast( OutputMemoryStream& inStream )
{
	LockGuard guard( &mLock );

	for ( auto user : mUsers )
		user->Send( inStream );
}

void UserManager::Broadcast( PacketID inPacketID, IPacket* inPacket )
{
	OutputMemoryStream stream;
	stream.Write( inPacketID );

	inPacket->Write( stream );

	Broadcast( stream );
}

void UserManager::SetParticipation( bool inParticipation )
{
	LockGuard guard( &mLock );

	for ( auto user : mUsers )
		user->SetParticipation( inParticipation );
}

std::string UserManager::ConvertGlobalNickname( const std::string & nickname )
{
	LockGuard guard( &mLock );

	if ( mNicknameOverlayCountMap.find( nickname ) == mNicknameOverlayCountMap.end() )
		return nickname;

	int count = mNicknameOverlayCountMap[nickname];
	return count == 0 ? nickname : nickname.c_str() + std::to_string( mNicknameOverlayCountMap[nickname] );
}

void UserManager::AddNicknameOverlayCount( const std::string& nickname )
{
	LockGuard guard( &mLock );

	if ( mNicknameOverlayCountMap.find( nickname ) == mNicknameOverlayCountMap.end() )
	{
		mNicknameOverlayCountMap[nickname] = 0;
		return;
	}

	++mNicknameOverlayCountMap[nickname];
}

void UserManager::SubNicknameOverlayCount( const std::string& nickname )
{
	LockGuard guard( &mLock );

	if ( mNicknameOverlayCountMap.find( nickname ) == mNicknameOverlayCountMap.end() )
	{
		mNicknameOverlayCountMap[nickname] = 0;
		return;
	}

	mNicknameOverlayCountMap[nickname] = std::min( mNicknameOverlayCountMap[nickname] - 1, 0 );
}
