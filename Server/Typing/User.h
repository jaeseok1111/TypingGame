#pragma once

class User : public Session
{
public:
	User() = default;

	void					SetParticipation( bool value ) { mIsParticipation = value; }
	bool					IsParticipation() const { return mIsParticipation; }

	void					SetNickname( const std::string& inNickname ) { mNickname = inNickname; }
	const std::string&		GetNickname() { return mNickname; }
	
private:
	Session*				Clone() override;

	void					OnConnect() override;
	void					OnDisconnect() override;

private:
	bool					mIsParticipation; // 게임 참여 여부
	
	std::string				mNickname;
};

