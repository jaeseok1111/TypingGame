#pragma once
class ChatMessage : public Job
{
public:
	ChatMessage( Session* inSession ) : Job( inSession ) {}

	Job*			Clone( Session* inSession ) override;

	bool			OnRead( InputMemoryStream& inStream ) override;
	void			OnExecute() override;

private:
	MessagePacket	mPacket;
};