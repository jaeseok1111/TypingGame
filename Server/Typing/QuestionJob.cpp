#include "stdafx.h"
#include "User.h"
#include "UserManager.h"
#include "QuestionManager.h"
#include "Ranking.h"
#include "QuestionJob.h"

// 아래 순서를 반복한다.
// 1. 대기 (QuestionWaitJob)
// 2. 문제 출제 (QuestionJob)
// 3. 랭킹 보여주기 (ShowQuestionRankingJob)

void QuestionWaitJob::OnTick()
{
	// 시스템 메세지
	MessagePacket packet;
	packet.Message = "5초 뒤에 문제가 출제됩니다.";

	LOG( "%s", packet.Message.c_str() )

	UserManager::Get()->Broadcast( PacketID::SystemMessage, &packet );

	// 다음 이어지는 잡 추가 ( 문제 출제 )
	JobManager::Get()->AddTimeJob( new QuestionJob( nullptr, 5000 ) );
}

void QuestionJob::OnTick()
{
	// 랜덤으로 문제를 가져온다
	if ( false == QuestionManager::Get()->CreateRandomAnswer() )
		return;

	auto currentAnswer = QuestionManager::Get()->NowAnswer();

	// 랭킹 기록 시작
	RankingRecoder::Get()->Start( currentAnswer );
	
	// 현재 접속되어있는 유저들만 참여
	UserManager::Get()->SetParticipation( true );

	// 시스템 메세지
	MessagePacket packet;
	packet.Message = "문제: " + currentAnswer;

	LOG( "%s", packet.Message.c_str() )

	UserManager::Get()->Broadcast( PacketID::SystemMessage, &packet );

	// 다음 이어지는 잡 추가 ( 랭킹 결과 보여주기 )
	JobManager::Get()->AddTimeJob( new ShowQuestionRankingJob( nullptr, 30000 ) );
}

void ShowQuestionRankingJob::OnTick()
{
	// 랭킹 결과 보내기
	RankGroupPacket rankGroup;
	rankGroup.Rankings = RankingRecoder::Get()->GetRankingToAnswer();

	UserManager::Get()->Broadcast( PacketID::ShowRanking, &rankGroup );
	
	// 유저 참여여부 false
	UserManager::Get()->SetParticipation( false );

	// 랭킹 기록 종료
	RankingRecoder::Get()->End();

	// 시스템 메세지
	MessagePacket packet;
	packet.Message = "1분뒤에 다시 시작됩니다.";

	LOG( "%s", packet.Message.c_str() )

	UserManager::Get()->Broadcast( PacketID::SystemMessage, &packet );

	// 다음 이어지는 잡 추가 ( 다시 시작 )
	JobManager::Get()->AddTimeJob( new QuestionWaitJob( nullptr, 60000 ) );
}
