#pragma once

class QuestionManager : public Singleton<QuestionManager>
{
public:
	bool						Load( const std::wstring& path );

	bool						CreateRandomAnswer();

	const std::string&			NowAnswer();

private:
	std::string					mAnswer;
	std::vector<std::string>	mQuestions;
};

