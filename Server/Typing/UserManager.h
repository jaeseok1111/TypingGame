#pragma once

class User;

class UserManager : public Singleton<UserManager>
{
public:
	UserManager() = default;

	void					AddUser( User* inUser );
	void					RemoveUser( User* inUser );

	void					Broadcast( OutputMemoryStream& inStream );
	void					Broadcast( PacketID inPacketID, IPacket* inPacket );

	void					SetParticipation( bool inParticipation );

	void					AddNicknameOverlayCount( const std::string& nickname );
	void					SubNicknameOverlayCount( const std::string& nickname );

	std::string				ConvertGlobalNickname( const std::string& nickname );

private:
	SpinLock				mLock;

	std::vector<User*>		mUsers;

	using NicknameOverlayCountMap = std::unordered_map<std::string, int>;
	NicknameOverlayCountMap	mNicknameOverlayCountMap;
};

