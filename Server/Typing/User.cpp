#include "stdafx.h"
#include "UserManager.h"
#include "User.h"

Session* User::Clone()
{
	return new User();
}

void User::OnConnect()
{
	UserManager::Get()->AddUser( this );
}

void User::OnDisconnect()
{
	UserManager::Get()->RemoveUser( this );

	if ( mNickname.empty() )
		return;

	UserManager::Get()->SubNicknameOverlayCount( mNickname );

	MessagePacket packet;
	packet.Message = mNickname.c_str() + std::string( " ���� �����ϼ̽��ϴ�." );

	UserManager::Get()->Broadcast( PacketID::SystemMessage, &packet );
}