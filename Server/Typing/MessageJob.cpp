#include "stdafx.h"
#include "User.h"
#include "UserManager.h"
#include "QuestionManager.h"
#include "Ranking.h"
#include "MessageJob.h"

Job* ChatMessage::Clone( Session* inSession )
{
	return new ChatMessage( inSession );
}

bool ChatMessage::OnRead( InputMemoryStream& inStream )
{
	mPacket.Read( inStream );
	return false == mPacket.Message.empty();
}

void ChatMessage::OnExecute()
{
	auto user = static_cast< User* >( mSession );
	auto message = mPacket.Message;

	if ( message[0] == '#' )
	{
		if ( RankingRecoder::Get()->IsRecoding() && user->IsParticipation() )
		{
			RankingRecoder::Get()->Record( user->GetNickname(), &message[1] );
			user->SetParticipation( false );
		}
	}
	
	MessagePacket packet;
	packet.Message = user->GetNickname().c_str() + string( " : " ) + message;

	UserManager::Get()->Broadcast( PacketID::Message, &packet );
}