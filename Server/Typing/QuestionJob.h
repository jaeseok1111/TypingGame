#pragma once

class QuestionWaitJob : public TimeJob
{
public:
	QuestionWaitJob( Session* inSession, int32_t inTime ) : TimeJob( inSession, inTime ) {}

	void OnTick() override;
};

class QuestionJob : public TimeJob
{
public:
	QuestionJob( Session* inSession, int32_t inTime ) : TimeJob( inSession, inTime ) {}

	void OnTick() override;
};

class ShowQuestionRankingJob : public TimeJob
{
public:
	ShowQuestionRankingJob( Session* inSession, int32_t inTime ) : TimeJob( inSession, inTime ) {}

	void OnTick() override;
};