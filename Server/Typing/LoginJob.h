#pragma once

class LoginJob : public Job
{
public:
	LoginJob( Session* inSession ) : Job( inSession ) {}

	Job*			Clone( Session* inSession ) override;

	bool			OnRead( InputMemoryStream& stream ) override;
	void			OnExecute() override;

private:
	std::string		mNickname;
};