#include "Core.h"
#include "WorkerThread.h"


WorkerThread::WorkerThread()
	: mSession( nullptr )
	, mJobTimer( make_shared<JobTimer>() )
	, mBytes( 0 )
{
}

void WorkerThread::Run()
{
	int startTick = static_cast< int >( GetTickCount() );
	int currentTick;

	while ( IsRun() )
	{
		currentTick = static_cast< int >( GetTickCount() );
		int tick = currentTick - startTick;
		startTick = currentTick;

		int timeout = tick < 16 ? 1 : 0; // 16 tick = 최소단위
		GetQueuedCompletion( timeout );

		RunTimeJob();

		RunSessionToOperation();
	}
}

void WorkerThread::GetQueuedCompletion( int inTimeout )
{
	bool result = IoCompletionPort::Get()->Pop( &mSession, &mBytes, inTimeout );

	if ( result == false )
	{
		if ( mSession != nullptr )
		{
			mSession->Disconnect();
			mSession = nullptr;
		}
	}
}

void WorkerThread::RunTimeJob()
{
	mJobTimer->OnTick();
}

void WorkerThread::RunSessionToOperation()
{
	if ( mSession == nullptr )
		return;

	switch ( mSession->GetOperation() )
	{
		case Session::Operation::Accept:
		{
			mSession->ConvertBufferToAddress();
			mSession->OnConnect();

			NetworkManager::Get()->Connect( mSession );
			IoCompletionPort::Get()->Connect( mSession->GetSocket() );

			LOG( "machineID: %d / accept= %s", mSession->GetMachineID(), mSession->GetAddress().ToString().c_str() )

			// 연결과 동시에 무언가 받았다면 처리한다
				if ( mBytes > 0 )
					RunExecuteJob();

			mSession->WaitReceive();
			break;
		}
		case Session::Operation::Read:
		{
			if ( mBytes == 0 )
			{
				mSession->Disconnect();
				break;
			}

			RunExecuteJob();
			mSession->WaitReceive();
			break;
		}
		case Session::Operation::Send:
		{
			if ( mBytes == 0 )
			{
				mSession->Disconnect();
				break;
			}

			mSession->WaitReceive();
			break;
		}
	}
}

void WorkerThread::RunExecuteJob()
{
	if ( mSession == nullptr )
		return;

	InputMemoryStream stream( mSession->GetReceiveBuffer(), mBytes );

	PacketID packetID;
	stream.Read( packetID );

	auto job = JobPtr( JobManager::Get()->CloneJobToPacketID( packetID, mSession ) );
	if ( job == nullptr )
		return;

	if ( false == job->OnRead( stream ) )
		return;

	job->OnExecute();
}