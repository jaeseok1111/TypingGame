#include "Core.h"
#include "JobTimer.h"

void JobTimer::AddJob( TimeJob* inJob )
{
	auto time = inJob->GetTickTime();
	auto slot = FindTimeSlot( time );

	if ( slot == nullptr )
		slot = CreateTimeSlot( time );

	slot->AddJob( inJob );
}

void JobTimer::OnTick()
{
	int32_t nowTick = GetTickCount();

	for ( auto slot : mTimeSlots )
		slot->OnTick( nowTick );

	RemoveEmptySlots();
}

JobTimeSlotPtr JobTimer::CreateTimeSlot( int32_t inTime )
{
	auto slot = make_shared<JobTimeSlot>( inTime );
	mTimeSlots.push_back( slot );

	return slot;
}

JobTimeSlotPtr JobTimer::FindTimeSlot( int32_t inTime )
{
	if ( mTimeSlots.empty() )
		return nullptr;

	auto slot = find_if( mTimeSlots.begin(), mTimeSlots.end(), [ inTime ] ( JobTimeSlotPtr slot )
	{
		return slot->Compare( inTime );
	} );
	return slot == mTimeSlots.end() ? nullptr : *slot;
}

void JobTimer::RemoveEmptySlots()
{
	if ( mTimeSlots.empty() )
		return;

	mTimeSlots.erase( remove_if( mTimeSlots.begin(), mTimeSlots.end(), []( auto slot )
	{
		return slot->IsEmpty();
	} ), mTimeSlots.end() );
}
