#include "Core.h"
#include "Job.h"
#include "Server.h"
#include "Session.h"

Session::Session()
	: mMachineID( -1 )
	, mIsConnected( false )
	, mOperation( Operation::Accept )
	, mSocket( nullptr )
	, mReceivedBytes( 0 )
	, mSentBytes( 0 )
{
	memset( mReceiveBuffer, '\0', Session::MaxBufferSize );
	memset( mSendBuffer, '\0', Session::MaxBufferSize );
}

void Session::WaitAccept( TCPSocketPtr inListener )
{
	mSocket = SocketUtil::CreateTCPSocket( SocketAddressFamily::Inet );
	mOperation = Operation::Accept;
	mIsConnected = false;

	BOOL result = AcceptEx( inListener->GetHandle(),		// listen 소켓
							mSocket->GetHandle(),			// 새로 연결될 소켓
							( PVOID )mReceiveBuffer,		// 연결된 주소를 받을 버퍼
							0,								// 연결시 받을 데이터 크기, 0이면 데이터 수신 안함
							sizeof( SOCKADDR_IN ) + 16,		// 로컬 주소 받는 버퍼 크기, 최소 16바이트 이상
							sizeof( SOCKADDR_IN ) + 16,		// 수신할 총 데이터의 바이트 단위 크기
							&mReceivedBytes,				// 수신할 총 데이터의 바이트 단위 크기
							this );							// overlapped 구조체 포인터

	if ( result == FALSE )
	{
		int error = SocketUtil::GetLastError();

		// 작업 진행중
		if ( error == ERROR_IO_PENDING )
			return;

		SocketUtil::ReportError( "Session::WaitAccept::AcceptEx" );
	}
}

void Session::Disconnect()
{
	LockGuard guard( &mLock );

	if ( false == mIsConnected )
		return;

	mSocket->Close();
	mIsConnected = false;

	LOG( "machineID: %d / disconnect= %s", mMachineID, mAddress.ToString().c_str() )

	NetworkManager::Get()->Disconnect( this );
	OnDisconnect();
}

void Session::Send( const OutputMemoryStream& inStream )
{
	mOperation = Operation::Send;
	
	mSentBytes = inStream.GetLength();
	memcpy( mSendBuffer, inStream.GetBufferPtr(), mSentBytes );

	WSABUF buffer;
	buffer.buf = mSendBuffer;
	buffer.len = mSentBytes;

	if ( FALSE == WSASend( mSocket->GetHandle(), &buffer, 1, &mSentBytes, 0, this, nullptr ) )
	{
		int error = SocketUtil::GetLastError();

		// 작업 진행중
		if ( error == 0 || error == ERROR_IO_PENDING )
			return;

		SocketUtil::ReportError( "Session::Send" );
	}
}

bool Session::IsConnected()
{
	LockGuard guard( &mLock );
	return mIsConnected;
}

void Session::WaitReceive()
{
	mOperation = Operation::Read;
	memset( mReceiveBuffer, '\0', Session::MaxBufferSize );
	
	WSABUF buffer;
	buffer.buf = mReceiveBuffer;
	buffer.len = Session::MaxBufferSize;

	DWORD flag = 0;
	if ( FALSE == WSARecv( mSocket->GetHandle(), &buffer, 1, 0, &flag, this, nullptr ) )
	{
		int error = SocketUtil::GetLastError();

		// 작업 진행중
		if ( error == 0 || error == ERROR_IO_PENDING )
			return;

		SocketUtil::ReportError( "Session::ReadMessage" );
	}
}

void Session::ConvertBufferToAddress()
{
	SOCKADDR_IN local;
	SOCKADDR_IN remote;

	int localLength = 0;
	int remoteLength = 0;

	GetAcceptExSockaddrs( mReceiveBuffer,
						  0,
						  sizeof( SOCKADDR_IN ) + 16,
						  sizeof( SOCKADDR_IN ) + 16,
						  ( SOCKADDR** )&local,
						  &localLength,
						  ( SOCKADDR** )&remote,
						  &remoteLength );

	mAddress = SocketAddress( remote );
}
