#include "Core.h"
#include "NetworkManager.h"

NetworkManager::~NetworkManager()
{
	DisconnectAll();

	for ( auto session : mSessions )
		delete session;
	mSessions.clear();
}

void NetworkManager::Push( Session * inSession )
{
	LockGuard guard( &mLock );

	mSessions.push_back( inSession );
}

void NetworkManager::Connect( Session* inSession )
{
	LockGuard guard( &mLock );

	inSession->SetConnected( true );
	mConnections.push_back( inSession );
}

void NetworkManager::Disconnect( Session * inSession )
{
	LockGuard guard( &mLock );

	auto machineID = inSession->GetMachineID();

	mConnections.erase( remove_if( mConnections.begin(), mConnections.end(), [ machineID ] ( auto connection )
	{
		return connection->GetMachineID() == machineID;
	} ), mConnections.end() );

	mDisconnections.push_back( machineID );
}

void NetworkManager::DisconnectAll()
{
	for ( auto session : mConnections )
		session->Disconnect();

	mConnections.clear();
}

void NetworkManager::CheckForDisconnects()
{
	LockGuard guard( &mLock );

	if ( mDisconnections.empty() )
		return;

	for ( auto machineID : mDisconnections )
		mSessions[machineID]->WaitAccept( mListener );

	mDisconnections.clear();
}
