#include "Core.h"
#include "JobTimer.h"
#include "JobManager.h"

void JobManager::AddJob( PacketID inPacketID, Job* inJob )
{
	LockGuard guard( &mLock );

	if ( mJobs.find( inPacketID ) != mJobs.end() )
		return;

	mJobs[inPacketID] = JobPtr( inJob );
}

void JobManager::AddTimeJob( TimeJob* inJob )
{
	auto worker = IoCompletionPort::Get()->GetCurrentThread();
	if ( worker == nullptr )
		return;

	auto timer = worker->GetTimer();
	timer->AddJob( inJob );
}

Job* JobManager::CloneJobToPacketID( PacketID packetID, Session* inSession )
{
	LockGuard guard( &mLock );

	if ( mJobs.find( packetID ) == mJobs.end() )
		return nullptr;

	return mJobs[packetID]->Clone( inSession );
}