#pragma once

class Session;

class NetworkManager : public Singleton<NetworkManager>
{
public:
	~NetworkManager();

private:
	friend class Server;
	friend class Session;
	friend class WorkerThread;

	void						Push( Session* inSession );

	void						Connect( Session* inSession );
	
	void						Disconnect( Session* inSession );
	void						DisconnectAll();

	void						CheckForDisconnects();

	void						SetLitener( TCPSocketPtr inListener ) { mListener = inListener; }

	SpinLock					mLock;
	TCPSocketPtr				mListener;

	std::vector<Session*>		mSessions;
	std::vector<Session*>		mConnections;
	std::vector<int>			mDisconnections;
};