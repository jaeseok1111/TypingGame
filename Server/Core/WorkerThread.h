#pragma once
#include "Thread.h"
#include "JobTimer.h"

class Server;
class Session;

class WorkerThread : public Thread
{
public:
	WorkerThread();

	JobTimerPtr		GetTimer() { return mJobTimer; }

private:
	void			Run() override;

	void			GetQueuedCompletion( int inTimeout );

	void			RunExecuteJob();
	void			RunTimeJob();
	void			RunSessionToOperation();

private:
	DWORD			mBytes;
	Session*		mSession;

	JobTimerPtr		mJobTimer;
};

using WorkerThreadPtr = std::shared_ptr<WorkerThread>;