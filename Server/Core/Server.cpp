#include "Core.h"
#include "Session.h"
#include "WorkerThread.h"
#include "Server.h"

Server* Server::mSharedInstance = nullptr;

Server::Server()
	: mPrototype( nullptr )
{
	assert( mSharedInstance == nullptr );
	mSharedInstance = this;

	WSADATA data;
	WSAStartup( MAKEWORD( 2, 2 ), &data );
}

Server::~Server()
{
	Release();
}

void Server::Initialize( Session* inPrototype, uint16_t inPort )
{
	if ( SetConsoleCtrlHandler( ( PHANDLER_ROUTINE )ConsoleShutdownHandler, TRUE ) == FALSE )
	{
		LOG("Server::Start::SetConsoleCtrlHandler Error!!")
		return;
	}

	SetPrototype( inPrototype );

	CreateListener( inPort );
	CreateClients( 300 );

	IoCompletionPort::Get()->Initialize();
}

void Server::SetPrototype( Session* inPrototype )
{
	if ( inPrototype == nullptr )
		return;

	mPrototype = SessionPtr( inPrototype );
}

void Server::CreateListener( uint16_t inPort )
{
	mListener = SocketUtil::CreateTCPSocket( SocketAddressFamily::Inet );
	if ( mListener == nullptr )
		return;

	// AcceptEx를 사용할 경우, listen에서 자동적으로 accept를 받지 못하도록 한다.
	BOOL valid = TRUE;
	setsockopt( mListener->GetHandle(), SOL_SOCKET, SO_CONDITIONAL_ACCEPT, ( char* )&valid, sizeof( valid ) );

	mListener->Bind( SocketAddress( "127.0.0.1", inPort ) );
	mListener->Listen( SOMAXCONN );

	IoCompletionPort::Get()->Connect( mListener );
	NetworkManager::Get()->SetLitener( mListener );
}

void Server::CreateClients( int inClientCount )
{
	auto networkManager = NetworkManager::Get();

	for ( int i = 0; i < inClientCount; ++i )
	{
		auto session = mPrototype->Clone();
		session->SetMachineID( i );
		session->WaitAccept( mListener );

		networkManager->Push( session );
	}
}

void Server::Run()
{
	// 메인 스레드
	while ( true )
	{
		if ( _kbhit() )
		{
			// Esc
			if ( _getch() == 27 )
				break;
		}

		NetworkManager::Get()->CheckForDisconnects();
	}
}

void Server::Release()
{
	if ( mSharedInstance == nullptr )
		return;

	mSharedInstance = nullptr;

	IoCompletionPort::Get()->StopAll();

	NetworkManager::Get()->DisconnectAll();

	WSACleanup();
}

BOOL WINAPI Server::ConsoleShutdownHandler( DWORD dwCtrlEvent )
{
	switch ( dwCtrlEvent )
	{
		case CTRL_C_EVENT:
		case CTRL_BREAK_EVENT:
		case CTRL_CLOSE_EVENT:
		case CTRL_LOGOFF_EVENT:
		case CTRL_SHUTDOWN_EVENT:
		{
			if ( mSharedInstance != nullptr )
				mSharedInstance->Release();

			return TRUE;
		}
		default:
			return FALSE;
	}
}