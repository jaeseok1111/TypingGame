#pragma once
#include "WorkerThread.h"

class IoCompletionPort : public Singleton<IoCompletionPort>
{
public:
	IoCompletionPort();
	~IoCompletionPort();

	void			Initialize();

	void			Connect( TCPSocketPtr inSocket );

	void			StopAll();

	bool			Pop( Session** session, LPDWORD bytes, int timeout );

	WorkerThreadPtr GetCurrentThread();

private:
	HANDLE			mPort;

	SpinLock		mLock;

	using WorkerMap = std::unordered_map<DWORD, WorkerThreadPtr>;
	WorkerMap		mWorkerThreadMap;
};