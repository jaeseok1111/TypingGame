#include "Core.h"
#include "JobTimeSlot.h"

JobTimeSlot::JobTimeSlot( int32_t inTime )
	: mTime( inTime )
{
	mLastTickCount = static_cast< int32_t >( GetTickCount() );
}

void JobTimeSlot::AddJob( TimeJob* inTimeJob )
{
	if ( mTime != inTimeJob->GetTickTime() )
		return;

	mNewJobs.push_back( TimeJobPtr( inTimeJob ) );
}

void JobTimeSlot::OnTick( int32_t inCurrentTickCount )
{
	CreateNewJobs();

	if ( false == IsBeyondTime( inCurrentTickCount ) )
		return;
	
	for_each( mJobs.begin(), mJobs.end(), [] ( auto job ) { job->OnTick(); } );
	
	RemoveDeadJobs();
}

bool JobTimeSlot::IsBeyondTime( int32_t inCurrentTickCount )
{
	int tick = inCurrentTickCount - mLastTickCount;
	if ( tick < mTime )
		return false;

	mLastTickCount = inCurrentTickCount;
	return true;
}

void JobTimeSlot::CreateNewJobs()
{
	if ( mNewJobs.empty() )
		return;

	for ( auto newJob : mNewJobs )
		mJobs.push_back( newJob );

	mNewJobs.clear();
}

void JobTimeSlot::RemoveDeadJobs()
{
	mJobs.erase( std::remove_if( mJobs.begin(), mJobs.end(), [] ( auto job )
	{
		return false == job->IsAlive();
	} ), mJobs.end() );
}

bool JobTimeSlot::Compare( int32_t inTime )
{
	return mTime == inTime;
}

bool JobTimeSlot::IsEmpty() const
{
	return mJobs.empty() && mNewJobs.empty();
}