#pragma once

class Job;
class Session;
class WorkerThread;

class Server
{
public:
	Server();
	virtual ~Server();

	virtual void		Initialize( Session* inPrototype, uint16_t inPort );
	void				Run();

	void				Release();

private:
	void				SetPrototype( Session* inPrototype );

	void				CreateListener( uint16_t inPort );
	void				CreateClients( int inClientCount );

	static BOOL WINAPI	ConsoleShutdownHandler( DWORD dwCtrlEvent );

	SpinLock			mLock;
	TCPSocketPtr		mListener;

	SessionPtr			mPrototype;

	static Server*		mSharedInstance;
};