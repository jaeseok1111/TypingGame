#pragma once

class Job;
class Session;

class JobManager : public Singleton<JobManager>
{
public:
	void								 AddJob( PacketID inPacketID, Job* inJob );
	void								 AddTimeJob( TimeJob* inJob );

	Job*								 CloneJobToPacketID( PacketID packetID, Session* inSession );

private:
	SpinLock							 mLock;

	std::unordered_map<PacketID, JobPtr> mJobs;
};