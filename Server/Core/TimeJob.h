#pragma once
#include "Session.h"

class TimeJob
{
public:
	TimeJob( Session* inSession, int32_t inTime ) : mIsAlive( false ), mSession( inSession ), mTime( inTime ) {}

	virtual void	OnTick() {}
	virtual void	OnEnd() {}

	bool			IsAlive() const { return mIsAlive; }

protected:
	void			SetAlive( bool isAlive ) { mIsAlive = isAlive; }

	int32_t			GetTickTime() const { return mTime; }

	Session*		GetSession() const { assert( mSession != nullptr ); return mSession; }

private:
	friend class JobTimeSlot;
	friend class JobTimer;

	bool			mIsAlive;
	int32_t			mTime;
	Session*		mSession;
};

using TimeJobPtr = std::shared_ptr<TimeJob>;