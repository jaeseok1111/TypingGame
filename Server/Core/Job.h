#pragma once

#include "Session.h"

class Job
{
public:
	Job( Session* inSession ) : mSession( inSession ) {}
	virtual ~Job() = default;

	virtual Job*			Clone( Session* inSession ) = 0;

	virtual bool			OnRead( InputMemoryStream& inStream ) { return true; }
	virtual void			OnExecute() {}

protected:
	Session*				mSession;

	Session*				GetSession() const { assert( mSession != nullptr ); return mSession; }
};

using JobPtr = std::shared_ptr<Job>;