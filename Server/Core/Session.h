#pragma once
#include "MemoryStream.h"

class Session : public OVERLAPPED
{
public:
	enum { MaxBufferSize = 1024 };

	Session();
	virtual ~Session() = default;

	void				Disconnect();

	void				Send( const OutputMemoryStream& inStream );

	TCPSocketPtr		GetSocket() { return mSocket; }
	SocketAddress		GetAddress() { return mAddress; }

	bool				IsConnected();

	int					GetMachineID() const { return mMachineID; }

protected:
	virtual void		OnConnect() {}
	virtual void		OnDisconnect() {}

	virtual Session*	Clone() = 0;

private:
	friend class Server;
	friend class WorkerThread;
	friend class NetworkManager;

	void				WaitAccept( TCPSocketPtr inListener );

	void				WaitReceive();

	void				ConvertBufferToAddress();

	void				SetMachineID( int inMachineID ) { mMachineID = inMachineID; }

	void				SetConnected( bool inConnected ) { mIsConnected = inConnected; }

	char*				GetSendBuffer() { return mSendBuffer; }
	char*				GetReceiveBuffer() { return mReceiveBuffer; }

	enum class Operation { Accept, Read, Send, Disconnect };
	Operation			GetOperation() const { return mOperation; }

private:
	int					mMachineID;
	bool				mIsConnected;

	SpinLock			mLock;

	TCPSocketPtr		mSocket;
	SocketAddress		mAddress;

	Operation			mOperation;

	DWORD				mReceivedBytes;
	DWORD				mSentBytes;

	char				mReceiveBuffer[MaxBufferSize];
	char				mSendBuffer[MaxBufferSize];
};

using SessionPtr = std::shared_ptr<Session>;