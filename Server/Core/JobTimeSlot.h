#pragma once
#include "TimeJob.h"

class JobTimeSlot
{
public:
	JobTimeSlot( int32_t inTime );

	void					AddJob( TimeJob* inTimeJob );

	void					OnTick( int32_t inCurrentTickCount );

	bool					IsEmpty() const;

	bool					Compare( int32_t inTime );

private:
	bool					IsBeyondTime( int32_t inCurrentTickCount );
	
	void					CreateNewJobs();
	void					RemoveDeadJobs();

	int32_t					mTime;
	int32_t					mLastTickCount;

	std::vector<TimeJobPtr>	mJobs;
	std::vector<TimeJobPtr> mNewJobs;
};

using JobTimeSlotPtr = std::shared_ptr<JobTimeSlot>;