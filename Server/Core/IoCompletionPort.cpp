#include "Core.h"
#include "IoCompletionPort.h"

IoCompletionPort::IoCompletionPort()
{
	mPort = ::CreateIoCompletionPort( INVALID_HANDLE_VALUE, nullptr, 0, 0 );
}

IoCompletionPort::~IoCompletionPort()
{
	StopAll();

	CloseHandle( mPort );
}

void IoCompletionPort::Initialize()
{
	SYSTEM_INFO info;
	GetSystemInfo( &info );

	for ( DWORD i = 0; i < info.dwNumberOfProcessors; ++i )
	{
		auto worker = make_shared<WorkerThread>();
		worker->Start();

		mWorkerThreadMap[worker->GetID()] = worker;
	}
}

void IoCompletionPort::StopAll()
{
	if ( mWorkerThreadMap.empty() )
		return;

	for ( auto[id, worker] : mWorkerThreadMap )
		worker->Stop();
}

void IoCompletionPort::Connect( TCPSocketPtr inSocket )
{
	CreateIoCompletionPort( ( HANDLE )inSocket->GetHandle(), mPort, ( ULONG_PTR )nullptr, 0 );
}

bool IoCompletionPort::Pop( Session** session, LPDWORD bytes, int timeout )
{
	ULONG_PTR key;
	OVERLAPPED* overlapped;

	bool result = TRUE == GetQueuedCompletionStatus( mPort, bytes, &key, &overlapped, timeout );

	*session = ( Session* )overlapped;
	return result;
}

WorkerThreadPtr IoCompletionPort::GetCurrentThread()
{
	DWORD currentThreadID = GetCurrentThreadId();
	
	if ( mWorkerThreadMap.find( currentThreadID ) == mWorkerThreadMap.end() )
		return ( *mWorkerThreadMap.begin() ).second;

	return mWorkerThreadMap[currentThreadID];
}
