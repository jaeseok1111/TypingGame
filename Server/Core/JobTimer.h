#pragma once
#include "JobTimeSlot.h"

class JobTimer
{
public:
	JobTimer() = default;

	void						AddJob( TimeJob* inJob );

	void						OnTick();

private:
	JobTimeSlotPtr				CreateTimeSlot( int32_t inTime );
	JobTimeSlotPtr				FindTimeSlot( int32_t inTime );

	void						RemoveEmptySlots();

	std::vector<JobTimeSlotPtr>	mTimeSlots;
};

using JobTimerPtr = std::shared_ptr<JobTimer>;